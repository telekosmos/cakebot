package org.awesomik.bot.test

import org.awesomik.bot.util.*
import org.junit.After
import org.junit.AfterClass
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test

import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertFalse
import static org.junit.Assert.assertNotEquals
import static org.junit.Assert.assertThat
import static org.hamcrest.Matchers.*
import static org.hamcrest.CoreMatchers.*

/**
 * Test individual states
 */
class StateMachineJUnitTest {

  StateMachine sm
  StateMachineJUnitTest() {
  }

  @BeforeClass
  static void setUpClass() {
    println "@BeforeClass setUpClass"
  }

  @AfterClass
  static void tearDownClass() {
  }

  @Before
  void setUp() {
    // run once before EVERY test; classic beforeEach
    // TODO state machine is new, but State's keep their values from
    println('Setting/Resetting stateData...')
    sm = new StateMachine()
    sm
  }

  @After
  void tearDown() {
  }

  @Test
  void testTest() {
    assertFalse("should be false", false)
  }

  @Test
  void beforeInitialStateTest() {
    assertThat(sm.state.stateName, equalTo(State.NULL))
    assertThat(sm.state.countMoves(), equalTo(0))
    assertThat(sm.state.remainingMoves(), greaterThan(0))
    assertThat(sm.state.getStateData(), is(not(null)))
    assertThat(sm.state.remainingLives, equalTo(StateValue.MAX_LIVES))
  }

  @Test
  void initialTransitionTest() {
    sm.transition() // countMoves = 0->1
    def newState = sm.state
    assertEquals(newState.stateName, State.INITIAL)
    assertThat(newState.remainingLives(), equalTo(StateValue.MAX_LIVES))

    assertThat(newState.countMoves(), greaterThan(0))
    assertThat(newState.remainingMoves(), greaterThan(0))
    // Decrease lives for the first live
    assertThat(newState.remainingLives(), equalTo(StateValue.MAX_LIVES))
    // check number of combos less than number of tiles
    def numCombos = newState.stateData.numOfCombos
    assertThat(newState.stateData.numOfCombos, lessThan(newState.stateData.numOfTiles))
  }


  @Test
  void inPlayStateTest() {
    sm.transition().transition()
    def inPlayState = sm.state
    assertEquals(inPlayState.stateName, State.IN_PLAY)
    assertThat(inPlayState.getStateData().numOfCombos, lessThan(inPlayState.getStateData().numOfTiles))
    assertEquals(inPlayState.countMoves(), 2)
    assertThat(inPlayState.getStateData(), not(is(null)))
    assertThat(inPlayState.getStateData(), instanceOf(StateValue.class))
  }


  @Test
  void alwaysNumberOfCombosLessThanTiles() {
    sm.transition() // NULL -> INITIAL moves: 0->1
    // I DUNNO whay stateData is accessible if it is declared as PRIVATE
    assertThat(sm.state.stateData.numOfCombos, lessThan(sm.state.stateData.numOfTiles))
    assertThat(sm.state.stateName, equalTo(State.INITIAL))
    assertThat(sm.state.countMoves(), equalTo(1))

    def winCriteria = {state -> state.countMoves() >= 3},
        loseCriteria = {state -> state.remainingMoves() == 0}

    sm.transition(winCriteria, null) // INITIAL -> IN_PLAY moves 1->2
    assertThat(sm.state.stateName, equalTo(State.IN_PLAY))
    assertThat(sm.state.countMoves(), equalTo(2))
    assertThat(sm.state.stateData.numOfCombos, lessThan(sm.state.stateData.numOfTiles))

    sm.transition(winCriteria, null) // IN_PLAY -> IN_PLAY moves 2->3
    assertThat(sm.state.stateName, equalTo(State.IN_PLAY))
    assertThat(sm.state.countMoves(), equalTo(3))
    assertThat(sm.state.stateData.numOfCombos, lessThan(sm.state.stateData.numOfTiles))

    sm.transition(winCriteria, null) // IN_PLAY -> ? moves 3->4
    assertThat(sm.state.stateName, equalTo(State.WIN))
    assertThat(sm.state.countMoves(), equalTo(3))
    assertThat(sm.state.stateData.numOfCombos, lessThan(sm.state.stateData.numOfTiles))

    sm.transition(winCriteria, loseCriteria)
    assertThat(sm.state.stateName, equalTo(State.INITIAL))
    assertThat(sm.state.countMoves(), equalTo(0))
    assertThat(sm.state.stateData.numOfTiles, lessThanOrEqualTo(StateValue.MAX_TILES))
    assertThat(sm.state.stateData.numOfCombos, lessThan(sm.state.stateData.numOfTiles))

  }

  @Test
  void comboSetDifferentBetweenPlays() {
    sm.transition()
    def state = sm.state
    def combosetOne = state.stateData.combos.clone()
    sm = sm.transition()
    // state = sm.state
    def combosetTwo = state.stateData.combos.clone()

    assertNotEquals(combosetOne.toString(), combosetTwo.toString())
  }

  @Test
  void inPlayToInPlayTransitionTest() {
    def winCriteria = {state -> state.remainingMoves() > 143}
    sm.transition() // NULL -> INITIAl
    sm.transition(winCriteria).transition(winCriteria) // INITIAL -> IN_PLAY, IN_PLAY -> IN_PLAY
    def inPlayState = sm.state
    assertEquals(inPlayState.stateName, State.IN_PLAY)
    assertEquals(inPlayState.countMoves(), 3)
    assertThat(inPlayState.countMoves(), not(equalTo(1)))
    assertThat(inPlayState.remainingMoves(), greaterThan(0))
  }


  @Test
  void inPlayToLoseTransitionTest() {
    def loseCriteria = {state -> state.countMoves() >= 3}
    def winCriteria = {state -> state.remainingMoves() > 143}
    sm.transition().transition().transition(winCriteria, loseCriteria)

    def inLoseState = sm.transition(winCriteria, loseCriteria).state
    assertEquals(inLoseState.stateName, State.LOSE)
    assertThat(inLoseState.countMoves(), greaterThan(2))
    assertThat(inLoseState.remainingMoves(), greaterThan(0))
    // assertThat(inPlayState.countMoves(), not(equalTo(1)))
  }


  @Test
  void inPlayToWinTransitionTest() {
    def loseCriteria = {state -> state.countMoves() >= 13}
    def winCriteria = {state -> state.countMoves() >= 3}
    sm.transition().transition().transition(winCriteria, loseCriteria)

    def inWinState = sm.transition(winCriteria, loseCriteria).state
    assertEquals(inWinState.stateName, State.WIN)
    assertThat(inWinState.countMoves(), greaterThan(2))
    assertThat(inWinState.remainingMoves(), lessThan(StateValue.MAX_MOVES))
    assertThat(inWinState.remainingMoves(), greaterThan(StateValue.MIN_MOVES))
  }


  @Test
  void winToInitialTest() {
    def loseCriteria = {state -> state.countMoves() >= 13}
    def winCriteria = {state -> state.countMoves() >= 2}

    sm.transition().transition().transition(winCriteria, loseCriteria)
    assertThat(sm.state.stateName, equalTo(State.WIN))

    def inWinState = sm.transition(winCriteria, loseCriteria).state
    assertEquals(inWinState.stateName, State.INITIAL)
    assertEquals(inWinState.countMoves(), 0)
    assertEquals(inWinState.stateData.remainingLives, StateValue.MAX_LIVES)
  }


  @Test
  void loseToInitialTest() {
    sm.transition() // countMoves = 0->1
    def newState = sm.state
    assertEquals(newState.stateName, State.INITIAL)
    assertThat(newState.remainingLives(), equalTo(StateValue.MAX_LIVES))

    def loseCriteria = {state -> state.countMoves() >= 3}
    def winCriteria = {state -> state.countMoves() >= 12}

    sm.transition() // countMoves = 1->2
    sm.transition(winCriteria, loseCriteria).transition(winCriteria, loseCriteria) // countMoves = 2->3
    assertThat(sm.state.stateName, equalTo(State.LOSE))

    def inLoseState = sm.transition(winCriteria, loseCriteria).state
    assertEquals(inLoseState.stateName, State.INITIAL)
    assertEquals(inLoseState.countMoves(), 0)
    assertEquals(inLoseState.remainingLives(), StateValue.MAX_LIVES-1)
  }


  @Test
  void loseAllLivesToEndTest() {
    def loseCriteria = {state -> state.countMoves() >= 2}
    def winCriteria = {state -> state.countMoves() < -12}

    def st = sm.transition().transition()
      st = sm.transition(winCriteria, loseCriteria).transition()
    assertThat(st.state.stateName, equalTo(State.INITIAL))
    assertThat(st.state.remainingLives(), equalTo(StateValue.MAX_LIVES-1))

    st = sm.transition().transition(winCriteria).transition(winCriteria, loseCriteria)
    st = sm.transition()
    assertThat(st.state.stateName, equalTo(State.INITIAL))
    assertThat(st.state.remainingLives(), equalTo(StateValue.MAX_LIVES-2))

    st = sm.transition().transition(winCriteria).transition(winCriteria, loseCriteria)
    st = sm.transition()
    assertThat(st.state.stateName, equalTo(State.INITIAL))
    assertThat(st.state.remainingLives(), equalTo(StateValue.MAX_LIVES-3))

    st = sm.transition().transition(winCriteria).transition(winCriteria, loseCriteria)
    st = sm.transition()
    assertThat(st.state.stateName, equalTo(State.INITIAL))
    assertThat(st.state.remainingLives(), equalTo(StateValue.MAX_LIVES-4))

    st = sm.transition().transition(winCriteria).transition(winCriteria, loseCriteria)
    st = sm.transition()
    assertThat(st.state.stateName, equalTo(State.INITIAL))
    assertThat(st.state.remainingLives(), equalTo(0))

    st = sm.transition();
    assertThat(st.state.stateName, equalTo(State.END))
  }
}
