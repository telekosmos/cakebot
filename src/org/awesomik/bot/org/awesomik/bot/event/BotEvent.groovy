package org.awesomik.bot.event

import org.awesomik.bot.util.StateValue

interface BotListener {
    void botActionPerformed (BotEvent ev)
    void onPlayCompleted(BotEvent ev)
}

/**
 * Created by telekosmos on 6/06/17.
 */
class BotEvent {
    StateValue stateVal
}
