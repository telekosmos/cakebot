
package org.awesomik.bot

import groovy.json.JsonSlurper
import org.awesomik.bot.util.*
import org.awesomik.bot.util.event.*

/**
 * Simulates a session with a number of games with a limited number of lives each.
 * It uses a state machine to perform the game. On every transition of the state
 * machine, it notify listeners on the new state data. Listener can be a reactive
 * agent subscribed to a hot observable (which is this bot).
 */
class Bot extends Thread implements BotEventEmitter {

  StateMachine sm
  int numGames, i
  def stateNames = [:]
  String botUuid
  Object user

  private List<BotListener> listeners = new ArrayList<BotListener>()


  /**
   * Creates an instance of this bot for a number of game sessions with a number
   * of lives each
   * @param numGames the number of games for this session, bot run.
   */
  Bot(Object user, whichGame = 1, numGames = 1) {
    sm = new StateMachine()
    stateNames.put(State.NULL, 'NULL')
    stateNames.put(State.INITIAL, 'INITIAL')
    stateNames.put(State.IN_PLAY, 'IN_PLAY')
    stateNames.put(State.WIN, 'WIN')
    stateNames.put(State.LOSE, 'LOSE')
    stateNames.put(State.END, 'END')
    this.numGames = numGames
    this.i = whichGame

    botUuid = UUID.randomUUID().toString()
    this.user = user
  }


  int numOfListeners() {
    listeners.size()
  }

  BotEventEmitter addBotListener(BotListener toAdd) {
    // listeners.add(toAdd)
    listeners << toAdd
    this
  }


  BotEventEmitter removeBotListener(BotListener toRmv) {
    listeners.removeElement(toRmv)
    this
  }


  void clearListeners() {
    listeners.clear()
  }


  void notifyListeners(BotEvent ev) {
    listeners.each { it ->
      if (ev instanceof BotStateEvent)
        (BotListener)it.onStateEmitted(ev)
      else
        (BotListener)it.onEventEmitted(ev)
    }
  }


  @Override
  void run () {
    this.simGame()
  }


  /**
   * Run a game simulation
   * @param i just an id
   */
  private void simGame(int i=1) {
    def loseCriteria = {state -> state.remainingMoves() == 0}
    def winCriteria = {state -> state.remainingMoves() < -2}

    def stateInfo, state // , logger = BotLogger.getLoger(Bot.class.getName())
    println("Start game ${this.i}")

    // stateVal = sm.transition()
    sm.transition()
    while (sm.state.stateName != State.END) {
      // emitStateInfo(stateVal.state)
      state = sm.state
      stateInfo = [
        // gUuid: sm.gameId,
        // lives: state.remainingLives(),
        // combos: state.stateData.combos, // hashMap, set of combos
        uuid: this.botUuid.substring(0, 5),
        state: stateNames.get(state.stateName),
        moves: state.countMoves(),
        remainingMoves: state.remainingMoves(),
        numTiles: state.stateData.numOfTiles,
        numCombos: state.stateData.numCombos(),
        sumOfCombos: state.stateData.sumOfCombos(),
        combos: state.stateData.combos
      ]
      if (stateNames.get(state.stateName) == 'INITIAL') {
        stateInfo['lives'] = state.remainingLives()
        stateInfo['rule'] = sm.ruleName
        stateInfo['username'] = this.user.user
        stateInfo['country'] = this.user.country
      }

      // println("stateInfo: $stateInfo")
      // Emit state info: emitter.onNext(stateInfo)
      this.notifyListeners(new BotStateEvent(botUuid, stateInfo))
      sm.transition()
    }

    // Notify end of game
    listeners.each { it ->
      it.onPlayCompleted(new BotEvent(this.botUuid))
    }
  }


  def simGames() {
    1.upto(this.numGames, { i -> this.simGame(i)} )
  }

}
