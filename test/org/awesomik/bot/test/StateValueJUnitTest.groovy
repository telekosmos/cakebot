package org.awesomik.bot.test

import org.awesomik.bot.util.Combos
import org.awesomik.bot.util.StateValue
import org.awesomik.bot.util.State
import org.awesomik.bot.util.StateMachine
import org.junit.After
import org.junit.AfterClass
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test
import static org.junit.Assert.*

import static org.hamcrest.MatcherAssert.assertThat
import static org.hamcrest.CoreMatchers.*
import static org.hamcrest.Matchers.*

class StateValueJUnitTest {
  StateValue stateValue

  StateValueJUnitTest() {

  }

  @BeforeClass
  static void setUpClass() {
    println "@BeforeClass setUpClass"
  }

  @AfterClass
  static void tearDownClass() {
  }

  @Before
  void setUp() {
    // run once before EVERY test; classic beforeEach
    println('Setting/Resetting stateData...')
    stateValue = new StateValue()
  }

  @After
  void tearDown() {
  }

  // TODO add test methods here.
  // The methods must be annotated with annotation @Test. For example:
  //
  // @Test
  //  void hello() {}
  @Test
  void testTest() {
    assertFalse("should be false", false)
  }

  @Test
  void stateValueCreation() {
    assertNotNull(stateValue)
  }

  @Test
  void stateValueSimpleProps() {
    assertThat(stateValue.numOfTiles, is(lessThanOrEqualTo(StateValue.MAX_TILES)))
    assertThat(stateValue.remainingMoves, is(lessThanOrEqualTo(StateValue.MAX_MOVES)))
    assertThat(stateValue.remainingLives, equalTo(StateValue.MAX_LIVES))
    assertThat(stateValue.numOfCombos, equalTo(0))
  }

  @Test
  void stateValueInitialCombos() {
    assertThat(stateValue.combos.values(), everyItem(anyOf(equalTo(0), instanceOf(List.class))))
  }


  @Test
  void stateValueGeneratedCombos() {
    stateValue.generateCombos()
    // assertThat(stateData.previousCombos.values(), everyItem(anyOf(equalTo(0), everyItem(equalTo(0)))))
    assertThat(stateValue.numOfCombos, lessThan(stateValue.numOfTiles))
    assertThat(stateValue.combos.values(), everyItem(anyOf(greaterThanOrEqualTo(0), instanceOf(List.class))))
  }

  @Test
  void getRightCombo() {
    1.upto(5, {
      def combo = stateValue.selectRandomCombo()
      assertThat(combo, isIn(Combos.ALL_COMBOS))
    })
  }

  @Test
  void canUpdateAnElem() {
    def sample = [0, 9, 9, 0, 0]

    def add = { op1, op2 -> op1+op2 }
    def zeroMinus = { op1, op2 -> op1-op2 < 0? 0: op1-op2 }
    def mutate = { int combo, Closure c ->
      sample[combo] = c(sample[combo], 1)
    }

    0.upto(sample.size()-1, { i ->
      if (i%2==0)
        mutate(i, add)
      else
        mutate(i, zeroMinus)
    })
    assertThat(sample, equalTo([1, 8, 10, 0, 1]))
  }


  @Test
  void canAddCombo() {
    def add = { op1, op2 -> op1+op2 }
    def zeroMinus = { op1, op2 -> op1-op2 < 0? 0: op1-op2 }
    String combo
    def old
    stateValue.generateCombos()
    1.upto(5, {
      combo = stateValue.selectRandomCombo()
      old = (stateValue.combos[combo] instanceof List
        ? stateValue.combos[combo].clone()
        : stateValue.combos[combo]
      )
      stateValue.mutateCombo(combo, add)
      println("combos vs tiles: ${stateValue.numOfCombos}, ${stateValue.numOfTiles}")
      assertThat(stateValue.combos[combo], is(not(equalTo(old))))
      if (old instanceof List)
        assertThat(old.sum(), lessThan(stateValue.combos[combo].sum()))
      else
        assertThat(old, lessThan(stateValue.combos[combo]))
    })
  }


  void canRemoveCombo() {
    def zeroMinus = { op1, op2 -> op1-op2 < 0? 0: op1-op2 }
    String combo
    def old
    stateValue.generateCombos()
    1.upto(5, {
      combo = stateValue.selectRandomCombo()
      old = (stateValue.combos[combo] instanceof List
        ? stateValue.combos[combo].clone()
        : stateValue.combos[combo]
      )
      stateValue.mutateCombo(combo, zeroMinus)
      if (old instanceof List)
        assertThat(old.sum(), greaterThanOrEqualTo(stateValue.combos[combo].sum()))
      else
        assertThat(old, anyOf(greaterThan(stateValue.combos[combo]), equalTo(0)))
    })
  }

  @Test
  void canMutateMultipleCombos() {
    stateValue.generateCombos()
    def oldCombos = stateValue.combos.clone()
    1.upto(10, {
      stateValue.mutateCombos()
      assertThat(stateValue.combos, is(not(equalTo(oldCombos))))
      oldCombos = stateValue.combos.clone()
    })
  }


  @Test
  void statePlayYieldsDifferentComboset() {
    def state = new State()
    def stateVal1 = state.play().val().clone()
    def stateVal2 = state.play().val().clone()
    assertThat(stateVal1, is(not(equalTo(stateVal2))))
    assertThat(true, is(true))
  }


  @Test
  void sumOfCombos() {
    stateValue.generateCombos()
    def sumOfCombos = stateValue.sumOfCombos()
    println("combos: $stateValue.numOfCombos; tiles: $stateValue.numOfTiles; sumOfcombos: $sumOfCombos")

    assertThat(sumOfCombos, greaterThanOrEqualTo(stateValue.numOfCombos))
  }

  /**
   * Assert the StateValue.numOfCombos is right the number of combos in the board
   */
  @Test
  void rightNumOfCombos() {
    stateValue.generateCombos()
    def combos = stateValue.combos.values()
    def elems = combos.collect { it ->
      if (it instanceof Integer)
        it > 0? it: 0
      else {
        def pops = it.findAll { c -> c > 0 }
        def numPops = pops.size()
        numPops
      }
    }

    def numCombos = elems.sum()
    assertThat(numCombos, equalTo(stateValue.numCombos()))
  }

  /**
   * Assesrt the StateValue.sumOfCombos is right the sum of all combos (included pop)
   */
  @Test
  void rightSumOfCombos() {
    stateValue.generateCombos()
    def combos = stateValue.combos.values()
    def elems = combos.collect { it ->
      if (it instanceof Integer)
        it > 0? it: 0
      else {
        def pops = it.findAll { c -> c > 0 }
        def numPops = pops? pops.sum(): 0
        numPops
      }
    }

    def numCombos = elems.sum()
    assertThat(numCombos, equalTo(stateValue.sumOfCombos()))
  }


  @Test
  void numCombosLTEsumOfCombos() {
    def accum = []
    stateValue.generateCombos()
    20.times { it ->
      println "${stateValue.combos.values()},"
      def num = stateValue.numCombos()
      def sum = stateValue.sumOfCombos()
      assertThat(num, is(lessThanOrEqualTo(sum)))
      stateValue.mutateCombos()
    }
  }
}
