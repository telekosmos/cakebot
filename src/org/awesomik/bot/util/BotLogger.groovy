package org.awesomik.bot.util

import org.apache.logging.log4j.Logger
import org.apache.logging.log4j.LogManager

import org.apache.logging.log4j.Level
import org.apache.logging.log4j.core.config.Configurator

class BotLogger {

  final Logger logger
  static Logger getLoger (String c) {
    Configurator.setLevel(c, Level.INFO)
    return LogManager.getLogger(c)
  }

  BotLogger(loggerName = "Bot") {
    this.logger = LogManager.getFormatterLogger(loggerName)
  }


  def info(msg) {
    logger.info(msg)
  }
}
