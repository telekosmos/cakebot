package org.awesomik.bot.util

/**
 * Utility class to define criteria for win/lose as static methods
 */
class WinLoseRule {

  private static Closure byLargeCombo = { state, comboAmount ->
    def movesThrs = state.remainingMoves() - state.countMoves() < 0
    def theCombos = state.stateData.combos
    def flatCombos = theCombos instanceof Map? theCombos.values().flatten()
      : theCombos.flatten()
    def bigCombo = flatCombos.any { it -> it >= comboAmount }

    movesThrs && bigCombo
  }

  private static Closure byManyCombos = { state, zeroCombos ->
    def movesThrs = state.remainingMoves() - state.countMoves() < 0
    def theCombos = state.stateData.combos
    def flatCombos = theCombos instanceof Map? theCombos.values().flatten()
      : theCombos.flatten()
    def numZeroCombos = flatCombos.inject(0) { accum, it ->
      it == 0? accum+1: accum
    }
    // println "zs: $numZeroCombos"
    movesThrs && numZeroCombos < zeroCombos
  }


  private static Closure byColorPop = { state ->
    def movesThrs = state.remainingMoves() - state.countMoves() < 0
    def theCombos = state.stateData.combos instanceof Map? state.stateData.combos.values()
      : state.stateData.combos
    def fullPops = theCombos.any { it ->
      if (it instanceof List)
        it.every { c -> c > 0}
    }
    movesThrs && fullPops
  }

  private static rules = [
    byColorPop, byManyCombos, byLargeCombo
  ]


  /**
   * Returns true if difference between remaining moves and moves is lt 0
   * @param state
   */
  static lessThanHalfMoves () {
    Closure ltHalfMoves = { state ->
      state.remainingMoves() - state.countMoves() < 0
    }
    ltHalfMoves
  }

  /**
   * Returns true if all pop combos of same color are bigger than 0
   * @param state
   */
  static winFullColorPop () {
    Closure winCriteria = byColorPop
    winCriteria
  }


  /**
   * Retrutn true if there is any combo with number gte comboAmount
   * @param state
   * @param comboAmount Default: 7
   */
  static winLargeComobo (int comboAmount = 7) {
    Closure winCriteria = this.byLargeCombo.rcurry(comboAmount)
    winCriteria
  }

  /**
   * Returns true if the number of zero combos is lte zeroCombos
   * @param state
   * @param zeroCombos Default: 19
   */
  static winManyCombos(int zeroCombos = 19) {
    Closure winCriteria = byManyCombos.rcurry(zeroCombos)
    winCriteria
  }

  /**
   * Returns a random win-lose rule
   */
  static getWinLoseRule() {
    def s = Math.random()*10000
    def i = new Double(s % 3).intValue()

    def chosen = rules[i]
    if (chosen.getMaximumNumberOfParameters() == 2) {
      if (i == 1)
        new Tuple2<Integer, Closure>(i, winManyCombos())
      else
        new Tuple2<Integer, Closure>(i, winLargeComobo())
    }
    else
      new Tuple2<Integer, Closure>(i, chosen)


  }


  static loseRule() {
    { it -> it.remainingMoves() == 0 }
  }
}
