package org.awesomik.bot.test

import org.awesomik.bot.util.*
import org.junit.After
import org.junit.AfterClass
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test

import static org.junit.Assert.assertFalse
import static org.junit.Assert.assertTrue

class WinLoseRuleJUnitTest {

  State state

  @BeforeClass
  static void setUpClass() {
    println "@BeforeClass setUpClass"
  }

  @AfterClass
  static void tearDownClass() {
  }

  @Before
  void setUp() {
    // run once before EVERY test; classic beforeEach
    // TODO state machine is new, but State's keep their values from
    println('Setting/Resetting stateData...')
    state = new State()
    state
  }

  @After
  void tearDown() {
    state = null
  }

  @Test
  void testTest() {
    assertFalse("should be false", false)
  }

  @Test
  void check50PctMoves() {
  }


  @Test
  void check50PctMovesAnd0s() {
    def winCriteria = WinLoseRule.winManyCombos(19)
    def mockCombos = [1, 3, 1, 0, 1, 0, 3, 0, 3, 3, 0, 2, [1, 1, 0, 2], [0, 1, 1, 1], [2, 0, 2, 0], [0, 0, 1, 0], [0, 1, 0, 1], [0, 0, 0, 1], 2, 1, 2, 0, 2, 0, 3, 1]
    state.setMoves(27, 11)
    state.stateData.combos = mockCombos
    assertTrue(winCriteria(state))

    state.setMoves(7, 32)
    assertFalse(winCriteria(state))

    state.setMoves(17, 17)
    assertFalse(winCriteria(state))

    state.setMoves(27, 17)
    mockCombos[12][0] = 0
    mockCombos[12][1] = 0
    assertFalse(winCriteria(state))
  }


  @Test
  void checkFullColorPop () {
    def winCriteria = WinLoseRule.winFullColorPop()
    def mockCombos = [1, 3, 2, 0, 1, 0, 3, 0, 3, 3, 0, 2, [1, 1, 2, 1], [0, 1, 0, 1], [2, 0, 2, 1], [0, 0, 1, 0], [0, 0, 0, 1], [0, 0, 0, 1], 2, 0, 0, 0, 2, 0, 3, 0]
    state.stateData.combos = mockCombos
    state.setMoves(17, 17)
    assertFalse(winCriteria(state))

    state.setMoves(27, 17)
    assertTrue(winCriteria(state))
  }


  @Test
  void checkLargeCombo() {
    def winCriteria = WinLoseRule.winLargeComobo()
    def mockCombos = [1, 3, 1, 0, 1, 0, 3, 0, 3, 3, 0, 2, [0, 0, 0, 0], [0, 1, 1, 1], [2, 0, 2, 0], [0, 0, 1, 0], [0, 1, 0, 1], [0, 0, 0, 1], 2, 6, 7, 0, 2, 0, 3, 1]
    state.stateData.combos = mockCombos
    state.setMoves(29, 28)
    assertTrue(winCriteria(state))

    mockCombos = [1, 3, 2, 0, 1, 0, 3, 0, 3, 3, 0, 2, [0, 0, 0, 0], [0, 1, 0, 1], [2, 0, 2, 1], [0, 0, 1, 0], [0, 0, 0, 1], [0, 0, 0, 1], 2, 0, 0, 0, 2, 0, 3, 0]
    state.stateData.combos = mockCombos
    assertFalse(winCriteria(state))
  }

}
