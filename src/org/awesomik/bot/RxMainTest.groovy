package org.awesomik.bot

import io.reactivex.*
import io.reactivex.disposables.Disposable

Flowable<Integer> flowableJust = Flowable.just(1, 5, 10)

flowableJust.subscribe { val ->
  // println("Subscriber received: ${val}")
}

println("================================")
Flowable<Integer> flowable = Flowable.create({ subscriber ->
  println("Started emitting")

  println("Emitting 1st")
  subscriber.onNext(1)

  println("Emitting 2nd")
  subscriber.onNext(2)

  subscriber.onComplete()
}, BackpressureStrategy.BUFFER)


println("Subscribing 1st subscriber")
flowable.subscribe (
  {val -> println("First Subscriber received: ${val}") },
  { val -> println("ERR!! First Subscriber received err: ${err}") },
  { println("First Subscriber: End event") }
)

println("=======================")

println("Subscribing 2nd subscriber")
flowable.subscribe { val -> println("Second Subscriber received: ${val}") }

// An observable has to "emit" something to observe...
Flowable<Integer> numberFlowable = Flowable.create({ emitter ->
  // @Override
  // public void subscribe(ObservableEmitter<Integer> emitter) throws Exception
  try {
    def stateData = [1, 2, 3, 4, 5]
    for (Integer data : stateData) {
      emitter.onNext(data)
    }
    emitter.onComplete()
  }
  catch (Exception e) {
    emitter.onError(e)
  }
}, BackpressureStrategy.BUFFER)
// Subscribe an observer to the observable
Disposable disposable = numberFlowable.subscribe({ n -> println("Number ${n}")})
disposable.dispose()

println("=== THREADING =======================")
/**
 * This example shows a custom Observable that does not block
 * when subscribed to as it spawns a separate thread.
 */
def customObservableNonBlocking(int id) {
  return Flowable.create({ subscriber ->
    Thread.start {
      8.times { i ->
        if (subscriber.isCancelled()) {
          return
        }
        subscriber.onNext("Thread-${id} value_${i}")
      }
      // after sending all values we complete the sequence
      if (!subscriber.isCancelled()) {
        subscriber.onComplete()
      }
    }
  } as FlowableOnSubscribe, BackpressureStrategy.BUFFER)
}

// To see output:
4.times { i ->
  customObservableNonBlocking(i).subscribe { println("What come from observable is: $it") }
}
