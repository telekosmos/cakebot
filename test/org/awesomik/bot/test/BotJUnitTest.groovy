
package org.awesomik.bot.test

import org.awesomik.bot.Bot
import org.awesomik.bot.util.State
import org.awesomik.bot.util.StateMachine
import org.awesomik.bot.util.event.BotEvent
import org.awesomik.bot.util.event.BotListener
import org.awesomik.bot.util.event.BotStateEvent
import org.junit.After
import org.junit.AfterClass
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test
import static org.junit.Assert.*

import static org.hamcrest.MatcherAssert.assertThat
import static org.hamcrest.CoreMatchers.*
import static org.hamcrest.Matchers.*


class BotJUnitTest {
  StateMachine sm
  Bot bot
  BotListener botListener1, botListener2
  Object user = new Object() {
    def id = 'myUser'
    def email = 'myuser@test.com'
  }

  BotJUnitTest() {
  }

  @BeforeClass
  static void setUpClass() {
    println "@BeforeClass setUpClass"
  }

  @AfterClass
  static void tearDownClass() {
  }

  @Before
  void setUp() {
    // run once before EVERY test; classic beforeEach
    // sm = new StateMachine()
    bot = new Bot(user)
    botListener1 = new BotListener() {
      @Override
      void onStateEmitted(BotStateEvent ev) {
        println("botListner1.onStateEmitted")
      }

      @Override
      void onEventEmitted(BotEvent ev) {
      }

      @Override
      void onPlayCompleted(BotEvent ev) {

      }
    }
    botListener2 = new BotListener() {
      @Override
      void onStateEmitted(BotStateEvent ev) {
      }

      @Override
      void onEventEmitted(BotEvent ev) {
      }

      @Override
      void onPlayCompleted(BotEvent ev) {
      }
    }
  }

  @After
  void tearDown() {
  }
  
  // TODO add test methods here.
  // The methods must be annotated with annotation @Test. For example:
  //
  // @Test
  //  void hello() {}
  @Test
  void testTest() {
    assertFalse("should be false", false)
  }


  @Test
  void addListenerToBot() {
    assertEquals(bot.numOfListeners(), 0)
    bot.addBotListener(botListener1)
    assertEquals(bot.numOfListeners(), 1)
  }


  @Test
  void removeListenerFromBot() {
    bot.addBotListener(botListener1)
    assertEquals(bot.numOfListeners(), 1)
    bot.removeBotListener(botListener1)
    assertEquals(bot.numOfListeners(), 0)
  }


  @Test
  void noListenersAfterClearing() {
    bot.addBotListener(botListener1).addBotListener(botListener2)
    assertEquals(2, bot.numOfListeners())
    bot.clearListeners()
    assertEquals(0, bot.numOfListeners())
  }


  /*
  @Test
  void stateMachineCreation() {
    assertNotNull("should exists state machine", sm)
  }

  @Test
  void initStateTest() {
    // assertThat(10L, greaterThan(9L))
    assertEquals(State.MAX_LIVES, sm.state.lives)
    assertThat(sm.state.moves, greaterThanOrEqualTo(State.MIN_MOVES))
    assertThat(sm.state.moves, lessThanOrEqualTo(State.MAX_MOVES))
    assertThat(sm.state.listCombos.size(), greaterThan(0))
    assertThat(sm.state.listCombos.values(), everyItem(equalTo(0)))
  }


  @Test
  void shouldDecreaseMovesBy1() {
    assertTrue("should yield a vertical green arrow combo", true)
    def moves = sm.state.moves
    sm.nextState()
    assertEquals(moves-1, sm.state.moves)
  }

  @Test
  void thereShouldBeAnyCombo() {
    sm.nextState()
    assertThat(sm.state.listCombos.values(), hasItem(not(equalTo(0))))
  }

  @Test
  void shouldCombosBeLessThanTiles() {

  }

/*
  @Test
   void greenHorizArrowTest() {
    assertTrue("should yield a horizontal green arrow combo", false)
  }
  
  @Test
   void greenBombTest() {
    assertTrue("should exists/yields a green bomb combo", false)
  }
  
  @Test
   void greenPop1Test() {
    assertTrue("should exists/yields a green pop 1 combo", true)
  }
  
  @Test
   void greenPop2Test() {
    assertTrue("should exists/yields a green pop 2 combo", false)
  }
*/
}
