package org.awesomik.bot

import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import org.awesomik.bot.util.event.BotEvent
import org.awesomik.bot.util.event.BotListener
import org.awesomik.bot.util.event.BotStateEvent

import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import java.util.zip.ZipFile

class Simul {
  def userStore = []
  // private final FILENAME = '/home/telekosmos/Dev/java/CakeSim/Bot/users.zip'
  private final FILENAME = '/Users/telekosmos/DevOps/java/CakeSim/Bot/users.zip'
  private final MAX_USER_INDEX = 210
  private final int MIN_BOTS_PER_ITERATION = 10
  private final int Max_BOTS_PER_ITERATION = 15

  Simul(maxUsers = 210) {
    // TODO: FILENAME to be read from config props or so
    File f = new File(this.FILENAME)
    def zf = new ZipFile(f)
    def usersZipEntry = zf.entries().find { it -> it.name.indexOf('users') == 0 }

    def users = new JsonSlurper().parse(zf.getInputStream(usersZipEntry))
    def left = MAX_USER_INDEX
    maxUsers.times { i ->
      // def userNumber = new Double(Math.random()*this.MAX_USER_INDEX).intValue()
      def userNumber = new Double(Math.random() * left).intValue()
      userStore.add(users.remove(userNumber))
      left--
    }
  }

  /**
   * Create a bot and add listeners to it
   * @return
   */
  def createBot(int i) {
    // select user
    def userNumber = new Double(Math.random() * userStore.size()).intValue()
    def user = userStore.get(userNumber)
    println("* username: ${user.user}")
    Bot bot = new Bot(userStore.get(userNumber), i)

    String filename = "${bot.botUuid.substring(0, 5)}-botlog.log.json"
    def rootFolder = this.FILENAME.substring(0, this.FILENAME.lastIndexOf('/'))
    def logFile = new File(rootFolder + '/log_users', filename)
    if (!logFile.exists())
      logFile.createNewFile()

    def os = logFile.newOutputStream()
    os << "[\n"
    def url = 'http://localhost:4400/api/misc/ping'.toURL()
    def listMoves = [], first = true
    bot.addBotListener(new BotListener() {
      @Override
      void onStateEmitted(BotStateEvent ev) {
        Map sv = new LinkedHashMap(ev.data)
        // println("stateValue ${sv.values()}")
        // listMoves << sv
        // listMoves.add(sv.values()) // this is to yield a json array :-S
        if (first) {
          os << JsonOutput.toJson(sv)
          first = false
        } else
          os << ",\n${JsonOutput.toJson(sv)}"
        /*
        def conn = url.openConnection()
        if (conn.responseCode == 200) {
          // def obj = new JsonSlurper().parse(url)
          // def obj = JsonOutput.toJson(sv)
          listMoves << sv
          // os << "${obj},\n"
        }
        else {
          println "Err on '${url.toString()}': (${conn.responseCode}) - ${conn.responseMessage}"
        }
        */
      }

      @Override
      void onEventEmitted(BotEvent ev) {
      }

      @Override
      void onPlayCompleted(BotEvent ev) {
        println "End of game received"
        // def jsonOut = JsonOutput.toJson(listMoves)
        // os << jsonOut
        os << "\n]"
        os.close()
        // emitter.onComplete()
      }
    })

    bot
  }

  /**
   * Starts up a simulation involving a number of users
   * @param {Integer} numUsers the number of users, players
   * @return
   */
  def simulation(numUsers = 24) {
    // println("Starting threads!!")
    // createBot().simGames()

    (numUsers).times { i ->
      println("Thread $i")
      createBot(i).start()
      /*
      Thread.start {
        createBot().simGame(i)
      }
      */
    }

    println('END!!!')
  }

  /**
   * Runs a full simulation by creating users -then games- in random intervals to simulate people
   * running games along the time.
   */
  def fullSim() {
    def game = new Runnable() {
      @Override
      void run() {
        createBot().simGame()
      }
    }
  }


  private shutdownSafely(ExecutorService pool) {
    pool.shutdown() // Disable new tasks from being submitted
    try {
      // Wait a while for existing tasks to terminate
      if (!pool.awaitTermination(60, TimeUnit.SECONDS)) {
        pool.shutdownNow() // Cancel currently executing tasks
        // Wait a while for tasks to respond to being cancelled
        if (!pool.awaitTermination(60, TimeUnit.SECONDS))
          println("*** Pool did not terminate ***")
      }
    }
    catch (InterruptedException ie) {
      // (Re-)Cancel if current thread also interrupted
      pool.shutdownNow()
      // Preserve interrupt status
      Thread.currentThread().interrupt()
    }
  }

  /**
   * Run a simulation of multiple game sessions and/or during a fixed amount of seconds
   * It performs the simulation by producing bots for users (players in sessions of 5 game loses)
   * as much as every second (interval between 0 and 1) in a ration of 15-25 game sesions each
   * @param sessions the number of sessions
   * @param duration the simulation duration in seconds
   */
  def simulator(sessions = 1, duration = 30) {
    def interval = Math.random() * 1000
    def rnd = new Random((new Date().time))
    def bots = rnd.nextInt(6) + MIN_BOTS_PER_ITERATION
    def accum = 1

    ExecutorService executor = Executors.newCachedThreadPool()
    sessions.times { i ->
      println("** interval: $interval, bots: $bots")
      Thread.currentThread().sleep(interval.longValue());
      bots.times { b ->
        Bot bot = createBot(accum++)
        executor.execute(bot)
      }
      interval = Math.random() * 1000
      bots = rnd.nextInt(6) + MIN_BOTS_PER_ITERATION
    }
    this.shutdownSafely(executor)
  }

}