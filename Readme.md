

## Simple Match-3 game simulator

[__WIP!!!__]

This is a simple groovy application which will generate, in a mostly absolutely random way, the combos resulting on every move in a typical match-3 game like Cake Swap (TM).

It was conceived as sort of state machine, the states will be the set of combos after every move, featuring lose and win criteria, max number of moves, max lives and a range of tiles.

In any case a movement is defined and there is no treats positioning, the only interesting thing here is the combos (Cake Swap produces a fairly large number of combos compared to many other match-3 games competitors, which makes the game pretty joyful).

So the main value of an IN_PLAY state is the amount of combos for each kind. The generation process is mostly random with constraints in order yield (at least a bit) meaningful results. Below the picture shows the distribution of the combos for each move (darker means larger amount of combos for a particular combo).

![Distribution](combo-distribution.png)

In order to run, the best thing so far is using an IDE (it was developed with IntelliJ Idea CE, which is free) and importing the gradle file and run the org.awesomik.bot.Main class, which is really a Groovy script.

The intended purpose is generate huge amounts of data of a known domain in order to experiments with some big data infraestructure just like Spark or Kafka in different fashions: batch or stream.