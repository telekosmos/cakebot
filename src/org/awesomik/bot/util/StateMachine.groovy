package org.awesomik.bot.util

class StateMachine {
  State state
  String gameId
  String ruleName
  Closure winCondition, loseCondition

  StateMachine() {
    gameId = UUID.randomUUID().toString()

    // this.state = State.NULL //
    this.state = new State()
    def ruleTuple = WinLoseRule.getWinLoseRule()
    this.winCondition = ruleTuple.second
    this.loseCondition = WinLoseRule.loseRule()
    this.ruleName = ruleTuple.first == 0? 'COLOR_POPS'
      : ruleTuple.first == 1? 'FEW_ZERO_COMBOS'
      : 'LARGE_COMBO'
  }

  /**
   * Set a new win condition
   */
  def setWinCondition() {
    def ruleTuple = WinLoseRule.getWinLoseRule()
    this.winCondition = ruleTuple.second
    this.loseCondition = WinLoseRule.loseRule()
    this.ruleName = ruleTuple.first == 0? 'COLOR_POPS'
      : ruleTuple.first == 1? 'FEW_ZERO_COMBOS'
      : 'LARGE_COMBO'
  }

  /**
   * Transition function. This could be well a transition table. As the state property is an State enum,
   * the state value has to be updated every time the state transitions to a different state
   * @return
   */
  def transition(def winCriteria = null, def loseCriteria = null) {
    def moves = this.state.countMoves(),
      remaining = this.state.remainingMoves()
    def prevState = [
      stateName: this.state.stateName,
      moves: moves,
      remainingMoves: remaining
    ]

    winCriteria = winCriteria ?: this.winCondition // {state -> state.remainingMoves() > 0}
    loseCriteria = loseCriteria ?: this.loseCondition // {state -> state.remainingMoves() == 0}

    switch(this.state.stateName) {
      case State.NULL:
        this.state.stateName = State.INITIAL
        // Keep max lives when the game session starts
        this.state.resetStateData(false)
        break

      case State.INITIAL:
        if (this.state.remainingLives() == 0)
          this.state.stateName = State.END
        else {
          this.state.stateName = State.IN_PLAY
          // this.state.stateValue = State.INITIAL.stateValue
          this.state.play()
        }
        break

      case State.IN_PLAY:
        if (loseCriteria(this.state)) { // End by losing
          this.state.stateName = State.LOSE
          // this.state.resetStateData(true)
          // this.state.stateValue = prevState.stateValue
          this.state.setMoves(prevState['moves'], prevState['remainingMoves'])
          return this
        }
        // else if (moves%2 == 1 && moves > 3) { // WIN
        else if (winCriteria(this.state)) { // End by winning
          this.state.stateName = State.WIN
          // this.state.resetStateData(false) // TODO: not sure about this
          // this.state.stateValue = prevState.stateValue
          this.state.setMoves(prevState['moves'], prevState['remainingMoves'])
          return this
        }
        else { // No end, go on
          this.state.stateName = State.IN_PLAY
          // this.state.increaseMoves()
          this.state.play()
        }
        break

      case State.WIN:
        this.state.stateName = State.INITIAL
        this.state.resetStateData(false) // could be also true
        break

      case State.LOSE:
        this.state.stateName = State.INITIAL
        this.state.resetStateData(true)
        break

      default: this.state.play()
        break
    } // EO switch

    // First change state and then update the moves!!!
    if (prevState['stateName'] != State.WIN && prevState['stateName'] != State.LOSE
      && this.state.stateName != State.END)
      this.state.setMoves(moves+1, remaining-1)

    // emit(prevSate, this.state)
    // println(this.state.toString())
    this
  }


  /*
   * Mutate the state to give birth to a new state
   * @return
   *
  def nextState() {
    this.state.moves--
    if (this.state.moves > 0) {
      yieldCombos()
      normalizeState()
    }
    else { // restart game if lives > 0

    }


  }

  /*
   * Check the consistency of the current state
   *
  private void normalizeState() {
    // |combos| < |tiles|
    def numCombos = 0
    this.state.listCombos.each {
      numCombos += it.stateData
    }

    while (numCombos >= this.state.tiles) {
      def tempMap, maxVal = 0
      this.state.listCombos.each {
        maxVal = maxVal < it.stateData? it.stateData: maxVal
      }
      // tempMap = this.state.listCombos.collectEntries { key, val ->
      this.state.listCombos = this.state.listCombos.collectEntries { key, val ->
        val == maxVal? [key, val-1]: [key, val]
      }

      numCombos = 0
      this.state.listCombos.each {
        numCombos += it.stateData
      }
    }


  }


  /*
   * Yield a new list of combos given some constraints and probabilities
   * @return
   *
  private void yieldCombos() {
    def randGen = new Random((new Date()).getTime())
    def randResult, units, tenths

    Combo.HARROW_COLOR_COMBOS.each { it ->
      randResult = randGen.nextInt(100)
      if (randResult%2 == 0) {
        units = randResult % 10
        tenths = randResult.intdiv(10) // randResult.intValue() / 10
        this.state.listCombos[it as Integer] = (units+tenths)/3 as Integer
        // state.listCombos.put(it as Integer, (units+tenths)/3 as Integer)
      }
    }

    Combo.VARROW_COLOR_COMBOS.each { it ->
      randResult = randGen.nextInt(100)
      if (randResult%2 == 0) {
        units = randResult % 10
        tenths = randResult.intdiv(10) // randResult.intValue() / 10
        this.state.listCombos[it as Integer] = (units+tenths)/3 as Integer
        // state.listCombos.put(it as Integer, (units+tenths)/2 as Integer)
      }
    }

    Combo.POP_COLOR_COMBOS.each { it ->
      randResult = randGen.nextInt(100)
      if (randResult%3 == 0) {
        units = randResult % 10
        tenths = randResult.intdiv(10) // randResult.intValue() / 10
        this.state.listCombos[it as Integer] = (units+tenths)/3 as Integer
        // state.listCombos.put(it as Integer, (units+tenths)/2 as Integer)
      }
    }

    Combo.POP2_COLOR_COMBOS.each { it ->
      randResult = randGen.nextInt(100)
      if (randResult%5 == 0) {
        units = randResult % 10
        tenths = randResult.intdiv(10) // randResult.intValue() / 10
        this.state.listCombos[it as Integer] = (units+tenths)/3 as Integer
        // state.listCombos.put(it as Integer, (units+tenths)/2 as Integer)
      }
    }

    Combo.BOMB_COLOR_COMBOS.each { it ->
      randResult = randGen.nextInt(100)
      if (randResult%7 == 0) {
        units = randResult % 10
        tenths = randResult.intdiv(10) // randResult.intValue() / 10
        this.state.listCombos[it as Integer] = (units+tenths)/3 as Integer
        // state.listCombos.put(it as Integer, (units+tenths)/2 as Integer)
      }
    }

    // Star
    randResult = randGen.nextInt(100)
    if (randResult % 13 == 0) {
      this.state.listCombos.put(Combo.STAR, 1)
    }
    // PopStar
    randResult = randGen.nextInt(100)
    if (randResult % 23 == 0) {
      this.state.listCombos.put(Combo.POP_STAR, 1)
    }

    def combos = this.state.listCombos.findAll { it.stateData > 0 }
    println "combos: ${combos}"
    println "fullcombos: ${this.state.listCombos}"
  }
  */
}
