package org.awesomik.bot.util.event

import org.awesomik.bot.util.State
import org.awesomik.bot.util.StateValue


class BotEvent {
  String botId, evId

  BotEvent(String id) {
    this.botId = id
    this.evId = UUID.randomUUID().toString()
  }
}

/**
 * Custom event to emit play data from bot
 */
class BotStateEvent extends BotEvent {
  def data

  BotStateEvent(String botId, StateValue stateData) {
    super(botId)
    this.data = stateData
  }

  BotStateEvent(String botId, Map stateData) {
    super(botId)
    data = stateData
  }
}


class BotStateChangedEvent extends BotEvent {
  State prev, current

  BotStateChangedEvent(String botId, p, c) {
    super(botId)
    this.prev = p
    this.current = c
  }
}