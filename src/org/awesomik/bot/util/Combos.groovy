package org.awesomik.bot.util

class Combos {
  /*static final GREEN_VARROW = 0
  static final GREEN_HARROW = 1
  static final GREEN_BOMB = 2
  static final GREEN_POP = 3
  static final GREEN_POP2 = 4

  static final RED_VARROW = 10
  static final RED_HARROW = 11
  static final RED_BOMB = 12
  static final RED_POP = 13
  static final RED_POP2 = 14

  static final BLUE_VARROW = 20
  static final BLUE_HARROW = 21
  static final BLUE_BOMB = 22
  static final BLUE_POP = 23
  static final BLUE_POP2 = 24

  static final YELLOW_VARROW = 30
  static final YELLOW_HARROW = 31
  static final YELLOW_BOMB = 32
  static final YELLOW_POP = 33
  static final YELLOW_POP2 = 34

  static final PINK_VARROW = 40
  static final PINK_HARROW = 41
  static final PINK_BOMB = 42
  static final PINK_POP = 43
  static final PINK_POP2 = 44

  static final PURPLE_VARROW = 50
  static final PURPLE_HARROW = 51
  static final PURPLE_BOMB = 52
  static final PURPLE_POP = 53
  static final PURPLE_POP2 = 54
  
  static final STAR  = 100
  static final POP_STAR = 500*/

  static final GREEN_VARROW = 'GreenHorArr'
  static final GREEN_HARROW = 'GreenVerArr'
  static final GREEN_BOMB = 'GreenBomb'
  static final GREEN_POP = 'GreenPop'
  static final GREEN_POP2 = 4

  static final RED_VARROW = 'RedVerArrow'
  static final RED_HARROW = 'RedHorArrow'
  static final RED_BOMB = 'RedBomb'
  static final RED_POP = 'RedPop'
  static final RED_POP2 = 14

  static final BLUE_VARROW = 'BlueVerArrow'
  static final BLUE_HARROW = 'BlueHorArrow'
  static final BLUE_BOMB = 'BlueBomb'
  static final BLUE_POP = 'BluePop'
  static final BLUE_POP2 = 24

  static final YELLOW_VARROW = 'YellowVerArrow'
  static final YELLOW_HARROW = 'YellowHorArrow'
  static final YELLOW_BOMB = 'YellowBomb'
  static final YELLOW_POP = 'YellowPop'
  static final YELLOW_POP2 = 34

  static final PINK_VARROW = 'PinkVerArrow'
  static final PINK_HARROW = 'PinkHorArrow'
  static final PINK_BOMB = 'PinkBomb'
  static final PINK_POP = 'PinkPop'
  static final PINK_POP2 = 44

  static final PURPLE_VARROW = 'PurpleVerArrow'
  static final PURPLE_HARROW = 'PurpleHorArrow'
  static final PURPLE_BOMB = 'PurpleBomb'
  static final PURPLE_POP = 'PurplePop'
  static final PURPLE_POP2 = 54

  static final STAR  = 'Star'
  static final POP_STAR = 'PopStar'

  // ArrayLists
  /**
   * Horizontal arrow color combos
   */
  static final HARROW_COLOR_COMBOS = [
    GREEN_HARROW, RED_HARROW, BLUE_HARROW, YELLOW_HARROW, PINK_HARROW, PURPLE_HARROW
  ]

  /**
   * Vertical arrow color combos
   */
  static final VARROW_COLOR_COMBOS = [
    GREEN_VARROW, RED_VARROW, BLUE_VARROW, YELLOW_VARROW, PINK_VARROW, PURPLE_VARROW
  ]

  /**
   * Color pops
   */
  static final POP_COLOR_COMBOS = [
    GREEN_POP, RED_POP, BLUE_POP, YELLOW_POP, PINK_POP, PURPLE_POP
  ]
  /*
  static final POP2_COLOR_COMBOS = [
    GREEN_POP2, RED_POP2, BLUE_POP2, YELLOW_POP2, PINK_POP2, PURPLE_POP2
  ]
  */
  /**
   * Bomb cake color combo
   */
  static final BOMB_COLOR_COMBOS = [
    GREEN_BOMB, RED_BOMB, BLUE_BOMB, YELLOW_BOMB, PINK_BOMB, PURPLE_BOMB
  ]

  static final List<Integer> ALL_COMBOS = [
    HARROW_COLOR_COMBOS, VARROW_COLOR_COMBOS, POP_COLOR_COMBOS, BOMB_COLOR_COMBOS,
    STAR, POP_STAR
  ].flatten()

  static final TOTAL_COMBOS = ALL_COMBOS.size()

  /**
   * Grouped map for all combos:
   * ALL_COMBOS[hArrowCombos] = [...]
   */
  static final ALL_COMBOS_MAP = [
    hArrowCombos: HARROW_COLOR_COMBOS,
    vArrowCombos: VARROW_COLOR_COMBOS,
    popCombos: POP_COLOR_COMBOS,
    // pop2Combos: POP2_COLOR_COMBOS,
    bombCombos: BOMB_COLOR_COMBOS,
    starCombo: STAR,
    popStarCombo: POP_STAR
  ]
}
