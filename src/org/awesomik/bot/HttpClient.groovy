package org.awesomik.bot

/*
import org.apache.http.HttpEntity
import org.apache.http.client.methods.CloseableHttpResponse
import org.apache.http.client.methods.HttpGet
import org.apache.http.client.methods.HttpPost
import org.apache.http.client.utils.URIBuilder
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClients
import org.apache.http.util.EntityUtils


CloseableHttpClient httpclient = HttpClients.createDefault()
HttpGet httpGet = new HttpGet("http://localhost/")

URI uri = new URIBuilder()
  .setScheme('http')
  .setHost('localhost')
  .setPort(10101)
  .setPath('act')
  .setParameter('misc', 'hello')
  .setParameter('name','Source')
  .build()
HttpPost postReq = new HttpPost(uri)

CloseableHttpResponse response1 = httpclient.execute(httpGet)
CloseableHttpResponse postResp = httpclient.execute(postReq)
// The underlying HTTP connection is still held by the response object
// to allow the response content to be streamed directly from the network socket.
// In order to ensure correct deallocation of system resources
// the user MUST call CloseableHttpResponse#close() from a finally clause.
// Please note that if response content is not fully consumed the underlying
// connection cannot be safely re-used and will be shut down and discarded
// by the connection manager.
try {
  println(response1.getStatusLine())
  HttpEntity entity1 = response1.getEntity()
  // do something useful with the response body
  // and ensure it is fully consumed
  // EntityUtils.consume(entity1)
  long len = entity1.getContentLength()
  def is = entity1.getContent()
  def content = EntityUtils.toString(entity1)
  println "Content length: ${len}\n${content}"

  def entityPost = postResp.getEntity()
  content = EntityUtils.toString(entityPost)
  def w = entityPost.contentLength
  println "Len: ${content.length()} vs ${w}"
  println "Resp: ${content}"
}
finally {
  response1.close()
  postResp.close()
}
*/

println "****** api/ping request :-S ******"
def damnUrl = 'http://localhost:4400/api/ping'
// def url = new URL('http://localhost:4400')
// url.path = '/api/ping'
def url = damnUrl.toURL()

def conn = url.openConnection()
if (conn.responseCode == 200) {
  println "** ${url.toString()} -> Resp: ${conn.content.text}"
}
else {
  println "Err on '${url.toString()}': (${conn.responseCode}) - ${conn.responseMessage}"
}

url.path = '/api/calculate/sum'
url.query = 'left=31&right=19'
damnUrl = 'http://localhost:4400/api/calculate/sum?left=3&right=19'
conn = damnUrl.toURL().openConnection()
if (conn.responseCode == 200) {
  println "** ${url.toString()} -> Resp: ${conn.content.text}"
}
else {
  println "Err on '${url.toString()}': (${conn.responseCode}) - ${conn.responseMessage}"
}
