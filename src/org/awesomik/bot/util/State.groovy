package org.awesomik.bot.util

/**
 * State of the game  based on the moves (updating combos, moves and lives according
 */
class State {
  // public static final String NULL="NULL", INITIAL="INITIAL", IN_PLAY="IN_PLAY", LOSE="LOSE", WIN="WIN", END="END"
  // public String stateName
  public static final int NULL=0, INITIAL=1, IN_PLAY=2, LOSE=3, WIN=4, END=5
  public int stateName

  private int moves = 0, remainingMoves = StateValue.MAX_MOVES, remainingLives = StateValue.MAX_LIVES
  private StateValue stateData


  State(lives=StateValue.MAX_LIVES, moves=0, tiles=StateValue.MAX_TILES) {
    this.moves = moves
    this.remainingLives = lives
    this.stateName = State.NULL
    this.stateData = new StateValue().generateCombos()
  }


  /**
   * Reset the state in different ways depending on the value of the state
   * value object and the parameter reset. If very first play, generate the combos set
   * no matter the parameter
   * @param reset if false (default) only reset the combos set; otherwise decrease lives and
   * generates a new set of combos
   * @return
   */
  def resetStateData(boolean reset=false) {
    // if (this.stateData == null) {
    if (this.moves == 0 && this.remainingLives == StateValue.MAX_LIVES) {
      // this.stateData = new StateValue()
      this.stateData.generateCombos()
    }
    else if(reset)
      this.stateData.reset()
    else
      this.stateData.resetCombos()

    this.remainingMoves = this.stateData.remainingMoves
    this.remainingLives = this.stateData.remainingLives
    this.moves = 0
    this
  }

  /**
   * Yields a new set of combos
   * @return
   */
  StateValue play() {
    def newData = this.stateData.mutateCombos() // it returns this.stateData though
    // this.moves++
    this.increaseMoves()

    this.stateData
  }

  int remainingLives() {
    this.remainingLives
  }

  StateValue getStateData() {
    this.stateData
  }

  int increaseMoves() {
    this.moves++
    this.remainingMoves--

    this.moves
  }

  int countMoves() {
    this.moves
  }

  int remainingMoves() {
    // def remaining = this.stateData?.remainingMoves
    // remaining == null? 0: remaining
    this.remainingMoves
  }


  void setMoves(int moves, int remaining) {
    this.moves = moves
    this.remainingMoves = remaining
  }

  String toString() {
    def header = "lives:tiles:moves:remaining:combos:sumcombos:combos:popcombos"
    def str = "l: ${this.remainingLives}; t: ${this.stateData.numOfTiles}; rm: ${this.remainingMoves}; "
    str += "c: ${this.stateData.numCombos()}; sc: ${this.stateData.sumOfCombos()}; "

    this.stateData.toMap().toString()
  }
}