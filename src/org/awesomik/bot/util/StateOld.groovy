package org.awesomik.bot.util


class StateClass {
  static final TILES = 1
  static final MOVES = 0

  static final MIN_TILES = 35
  static final MAX_TILES = 65
  static final MAX_LIVES = 5
  static final MIN_MOVES = 19
  static final MAX_MOVES = 45

  Integer lives
  Integer moves
  Integer tiles // board tiles

  // keeps a map just like comboType: numberOfThisComboType
  Map<Integer, Integer> listCombos = new HashMap<Integer, Integer>()

  StateClass() {
    Combos.ALL_COMBOS.each {entry ->
      entry.value.each { it ->
        listCombos.put(it, 0)
      }
    }
    lives = MAX_LIVES
    tiles = new Random().nextInt(MAX_TILES)
    tiles = tiles < MIN_TILES? MIN_TILES: tiles

    moves = new Random().nextInt(MAX_MOVES)
    moves = moves < MIN_MOVES? MIN_MOVES: moves
  }


  /**
   * Dumb initial generation for tiles or moves
   * @param what which generate
   * @return the amount of tiles or moves, int
   */
  static final int generateInit(int what) {
    switch (what) {
      case TILES: return Math.abs((MAX_TILES+MIN_TILES)/2)
      case MOVES: return Math.abs((MAX_MOVES+MIN_MOVES)/2)

      default: return Math.abs((MAX_TILES+MIN_TILES)/2)
    }
  }
}
