package org.awesomik.bot

import groovy.json.JsonOutput
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.FlowableOnSubscribe
import io.reactivex.disposables.Disposable
import org.awesomik.bot.Simul
import org.awesomik.bot.Bot
import org.awesomik.bot.util.BotLogger
import org.awesomik.bot.util.StateValue
import org.awesomik.bot.util.event.BotEvent
import org.awesomik.bot.util.event.BotListener
import org.awesomik.bot.util.event.BotStateEvent
import groovy.json.JsonSlurper
import groovy.json.JsonParser
import sun.security.krb5.internal.PAEncTSEnc
/*
def bot = new Bot()
String filename = "${bot.botUuid.substring(0,5)}-botlog.log"
BufferedWriter bw = new File('.', filename).newWriter()
def logFile = new File(filename)
def os = logFile.newOutputStream()
// println "Loggin' to ${filename}"
/*
def botListner = new BotListener() {
  @Override
  void onStateEmitted(BotStateEvent ev) {
    Map sv = ev.data
    // emitter.onNext(sv)
    println "evLog: ${sv}"
    // bw.writeLine("onStateEmitted cb: ${sv}")
    os << "onStateEmitted cb: ${sv}\n"
  }

  @Override
  void onEventEmitted(BotEvent ev) {
    print "onEventEmitted..."
  }

  @Override
  void onPlayCompleted(BotEvent ev) {
    println "End game received and closing output stream"
    os.close()
  }
}

bot.addBotListener(botListner)
bot.simGames()
*/

/*
// An observable has to "emit" something to observe...
Flowable botFlowable = Flowable.create({ emitter ->
  // @Override
  // public void subscribe(ObservableEmitter<Integer> emitter) throws Exception
  bot.addBotListener(new BotListener() {
    @Override
    void onStateEmitted(BotStateEvent ev) {
      Map sv = ev.data
      emitter.onNext(sv)
      // println("stateValue $sv")
    }

    @Override
    void onEventEmitted(BotEvent ev) {
      print "onEventEmitted..."
    }

    @Override
    void onPlayCompleted(BotEvent ev) {
      println "End game received and closing output stream"
      os.close()
      emitter.onComplete()
    }
  })
}, BackpressureStrategy.BUFFER)
// Subscribe an observer to the observable
// Disposable disposable =
botFlowable.subscribe({ it ->
  println "log: ${it}"
  // bw.writeLine("From subscriber: ${it}")
  os << "RxSubscriber: ${it}\n"
})

bot.simGames()
// disposable.dispose()
*/


final TYPE = 'http'
final HOST = 'localhost'
final PATH = '/api/misc'
final PORT = 4400
final COMMAND = 'ping'

final LOGPATH = '/home/telekosmos/Dev/java/CakeSim/Bot'

def damnUrl = "$TYPE://$HOST:$PORT$PATH/$COMMAND"
println "damnUrl: $damnUrl"
// def url = new URL('http://localhost:4400')
// url.path = '/api/ping'
def url = damnUrl.toURL()

/*def conn = url.openConnection() // IN conn I already have data back from the request
if (conn.responseCode == 200) {
  println "** ${url.toString()} -> Resp: ${conn.content.text}"
}
else {
  println "Err on '${url.toString()}': (${conn.responseCode}) - ${conn.responseMessage}"
}
*/


def createBot() {
  Bot bot = new Bot()
  // BufferedWriter bw = new File('.', "$bot.botUuid-botlog.log").newWriter()
  String filename = "${bot.botUuid.substring(0,5)}-botlog.log.json"
  def logFile = new File('/home/telekosmos/Dev/java/CakeSim/Bot/log_rule', filename)
  if (!logFile.exists())
    logFile.createNewFile()

  def os = logFile.newOutputStream()
  os << "[\n"
  def url = 'http://localhost:4400/api/misc/ping'.toURL()
  def listMoves = [], first = true
  bot.addBotListener(new BotListener() {
    @Override
    void onStateEmitted(BotStateEvent ev) {
      Map sv = new LinkedHashMap(ev.data)
      // println("stateValue ${sv.values()}")
      // listMoves << sv
      // listMoves.add(sv.values()) // this is to yield a json array :-S
      if (first) {
        os << JsonOutput.toJson(sv)
        first = false
      }
      else
        os << ",\n${JsonOutput.toJson(sv)}"
      /*
      def conn = url.openConnection()
      if (conn.responseCode == 200) {
        // def obj = new JsonSlurper().parse(url)
        // def obj = JsonOutput.toJson(sv)
        listMoves << sv
        // os << "${obj},\n"
      }
      else {
        println "Err on '${url.toString()}': (${conn.responseCode}) - ${conn.responseMessage}"
      }
      */
    }

    @Override
    void onEventEmitted(BotEvent ev) {
    }

    @Override
    void onPlayCompleted(BotEvent ev) {
      println "End of game received"
      // def jsonOut = JsonOutput.toJson(listMoves)
      // os << jsonOut
      os << "\n]"
      os.close()
      // emitter.onComplete()
    }
  })

  bot
}
/*
println("Starting thread!!")
24.times { i ->
  Thread.start {
    createBot().simGame(i)
  }
}

println('END!!!')
*/

def simulator = new Simul()
// simulator.simulation()
simulator.simulator(14, 30)