package org.awesomik.bot.util

/**
 * The state of the state
 */
class StateValue {
  int numOfTiles
  int remainingMoves
  int remainingLives

  static final int MAX_LIVES = 5
  static final int MAX_TILES = 9*9 // size of the board
  static final int MIN_TILES = 2*3*4
  static final int MAX_MOVES = 45
  static final int MIN_MOVES = 2*3*4

  // there will be 0.4 combos per tile, max
  static final double NUMCOMBOS_TILES_RATE = 0.4
  static final int MAX_SINGLE_COMBO = 6
  static final int NUM_OF_POPS = 4

  private add = { op1, op2 -> op1+op2 }
  private zeroMinus = { op1, op2 -> op1-op2 <= 0? 0: op1-op2 }

  // Combos
  def previousCombos = [:]
  def combos = [:]
  int numOfCombos, combosTilesThreshold // max num of combos regarding to num of tiles

  StateValue() {
    while ((this.numOfTiles = (int)Math.round(Math.random()*MAX_TILES)) <= MIN_TILES) {}
    while ((this.remainingMoves = (int)Math.round(Math.random()*MAX_MOVES)) <= MIN_MOVES) {}
    this.remainingLives = MAX_LIVES
    this.combosTilesThreshold = Math.floor((this.numOfTiles*NUMCOMBOS_TILES_RATE).doubleValue())

    this.resetCombos()
    // this.generateCombos()
    this.numCombos()
    // this.normalize()
    //  println('EO StateValue()')
  }

  StateValue(int tiles, int moves, int lives, Map combos) {
    this.numOfTiles = tiles
    this.remainingMoves = moves
    this.remainingLives = lives
    this.combos = combos
  }

  /**
   * Generates a random integer between 0 and limit
   * @param limit random generator upper limit
   * @return
   */
  private final int randInt(int limit) {
    return (int)Math.floor(Math.random()*limit)
  }

  /**
   * Creates and initializes all combos to 0
   */
  final StateValue resetCombos() {
    // initialize all combos to 0
    Combos.ALL_COMBOS.each { it ->
      // For each item in the map stateData (the list of color combos), set count to 0
      if (it in Combos.POP_COLOR_COMBOS) {
        // every item in the list is a pop with an accumulated stateData, set to 0
        def comboPops = []
        0.upto(NUM_OF_POPS-1, { i ->
          comboPops[i] = 0
        })
        this.combos[it] = comboPops
        this.previousCombos[it] = []
      }
      else {
        this.combos[it] = 0 // (int)Math.round(Math.random()*8)
        this.previousCombos[it] = 0
      }
    } // EO closure
    this.numCombos()
    this
  }

  /**
   * Generate combos in the "board" assuring there is less combos than tiles.
   * MIND a new set of combos is generated if the number of combos is less than a threshold
   * It assumes the number of combos is 0
   */
  final StateValue generateCombos() {
    def whichCombo, comboAmount, popIndex
    // println "[StateValue.generateCombos]: ${this.numOfCombos < this.combosTilesThreshold}"
    while (this.numOfCombos < this.combosTilesThreshold) {
      whichCombo = this.randInt(Combos.TOTAL_COMBOS)
      whichCombo = Combos.ALL_COMBOS[whichCombo]
      // whichCombo = whichCombo >= Combos.TOTAL_COMBOS? Combos.TOTAL_COMBOS-1: whichCombo

      if (whichCombo in Combos.POP_COLOR_COMBOS) {
        // increase num of pops for a random pop (pop(3) <- 4 in the end)
        popIndex = this.randInt(NUM_OF_POPS)
        this.combos[whichCombo][popIndex]++ // = comboAmount
        /*
        println("[StateValue.generateCombos] POP (whichPop:amount->howMany): " +
          "${whichCombo}:${popIndex}->${this.combos[whichCombo][popIndex]}")
         */
      }
      else { // increase the number of combos (i.e. horizontal_red_arrow++)
        this.combos[whichCombo]++
        // println("NORMAL: ${whichCombo}")
      }

      this.numCombos()
    }
    this
  }


  /**
   * Select a random combo
   * @return {String} Actually, the name of the combo
   */
  def selectRandomCombo() {
    def whichComboIndex
    whichComboIndex = this.randInt(Combos.TOTAL_COMBOS) // 0-TOTAL_COMBOS-1
    def combo = Combos.ALL_COMBOS[whichComboIndex]
    combo
  }


  /**
   * Mutate a combo by applying a closure
   * @param combo the index of a combo
   * @param c closure to apply to combo
   * @return this object
   */
  StateValue mutateCombo(String combo, Closure c) {
    if ((this.numOfCombos >= this.numOfTiles-this.combosTilesThreshold) && c == add) {
      // println("[StateValue.mutateCombo] return: ${this.numOfTiles-this.combosTilesThreshold} && add")
      return null
    }

    if (combo in Combos.POP_COLOR_COMBOS) {
      def popIndex = this.randInt(NUM_OF_POPS)
      // println("[StateValue.mutateCombo] POP before: ${combo}:${popIndex+1}->${this.combos[combo][popIndex]}")
      this.combos[combo][popIndex] = c(this.combos[combo][popIndex], 1)
      // println("[StateValue.mutateCombo] POP (whichPop:amount->howMany) ${combo}:${popIndex+1}->${this.combos[combo][popIndex]}")
    }
    else
      this.combos[combo] = c(this.combos[combo], 1)

    this.numCombos()
    this
  }


  /**
   * Yields a new set of combos. Removes a random number of combos and replaces them
   * by another random combos.
   * @return {StateValue} this object with the new combos
   */
  StateValue mutateCombos() {
    this.previousCombos = this.combos.clone()

    def affectedCombos = this.randInt(Math.round(this.numOfCombos*NUMCOMBOS_TILES_RATE).intValue())

    def combosToRemove, combosToUpdate,
        changeCombosThreshold = Math.floor(this.numOfCombos*0.25)
    // if ratio numOfCombos-numOfTiles is close to threshold, don't add and only remove
    if (this.numOfCombos >= this.numOfTiles-this.combosTilesThreshold) {
      combosToRemove = affectedCombos
      combosToUpdate = 0
    }
    else {
      for (i in 1..5) {
        combosToRemove = affectedCombos
        combosToUpdate = affectedCombos
        if (Math.abs(combosToUpdate-combosToRemove) > changeCombosThreshold)
          break
      }
    }

    // println("[StateValue.mutateCombos] toRemove: ${combosToRemove}; toUpdate: ${combosToUpdate}")
    // Remove a set of combos
    String combo
    1.upto(combosToRemove+1, {
      combo = selectRandomCombo()
      mutateCombo(combo, zeroMinus)
    })
    // then add another random set of combos
    1.upto(combosToUpdate+1, {
      combo = selectRandomCombo()
      mutateCombo(combo, add)
    })

    // TODO: check here if combos changes, are different than previousCombos
    // and then check if statedata in State.play changes
    return this
  }


  /**
   * Total amount of combos.
   * Pops account for as many they are (i.e. triple red pop sums up 3 for one tile occupied)
   * @return
   */
  int sumOfCombos() {
    def accum = 0

    this.combos.each { entry ->
      accum += ((String)entry.key) in Combos.POP_COLOR_COMBOS ?
        entry.value.sum():
        // entry.value.withIndex().inject(0, {c, it -> c+((it[1]+1) * it[0])}):
        entry.value
    }
    accum
  }


  /**
   * Gets the number of combos "on the board". Pops account only for one (tile occupied).
   * This is the stateData to take into account when checking vs number of tiles
   * @return
   */
  int numCombos() {
    def accum = 0
    this.combos.each { entry ->
      // println("numOfCombos vs tot ${this.numOfCombos} vs ${this.numOfTiles} :: ${entry.key} -> ${entry.stateData}")
      accum += ((String)entry.key) in Combos.POP_COLOR_COMBOS ?
        entry.value.inject(0) { c, it -> it == 0? c: c+1 }:
        // entry.value.sum():
        entry.value
    }
    this.numOfCombos = accum
    accum
  }


  /**
   * Reset the values, generating new tiles, moves and combos; decrease remaining lives
   * This is done to use just this class as a "factory" for consecutive "games".
   * If not used, a new StateValue object should be created every time a new game board is played
   * @return {StateValue} this object with lives decreased
   */
  StateValue reset() {
    while ((this.numOfTiles = this.randInt(StateValue.MAX_TILES)) <= MIN_TILES) {}
    while ((this.remainingMoves = this.randInt(StateValue.MAX_MOVES)) <= MIN_MOVES) {}
    this.remainingLives--
    this.combosTilesThreshold = Math.floor((this.numOfTiles*NUMCOMBOS_TILES_RATE).doubleValue())

    // this.mutateCombos()
    this.resetCombos()
    this.generateCombos()
    this
  }


  private List popCombos() {
    def list = []
    this.combos.each {entry ->
      if (((Integer)entry.key) in Combos.POP_COLOR_COMBOS)
        list << entry.value
    }
    list
  }


  def toMap() {
    return [
      'numOfLives': this.remainingLives,
      'numOfTiles': this.numOfTiles,
      'moves': this.remainingMoves,
      'combos': this.combos
      // 'popCombos': this.popCombos()
    ]
  }

  /**
   * Return a map of the current state of this object
   */
  def val() {
    def newVal = this.toMap()
    newVal['combos'] = this.combos.clone()

    newVal
  }

  // Factories if possible to have two chances for new objects
  static StateValue factory() {
    return new StateValue()
  }

  static StateValue factory(int tiles, int moves, int lives, Map combos) {
    return new StateValue(tiles, moves, lives, combos)
  }

}
