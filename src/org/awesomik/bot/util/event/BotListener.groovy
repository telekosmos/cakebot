package org.awesomik.bot.util.event


interface BotListener {
  void onStateEmitted(BotStateEvent ev)
  void onEventEmitted(BotEvent ev)
  void onPlayCompleted(BotEvent ev)
}


interface BotEventEmitter {
  BotEventEmitter addBotListener(BotListener listener)
  BotEventEmitter removeBotListener(BotListener listener)
  void clearListeners()
  void notifyListeners(BotEvent ev)
}